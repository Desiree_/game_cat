## Programme vorstellen
Um dieses Spiel zu entwickeln brauche ich natürlich Applikationen die mit dabei helfen. Die wichtigsten Programme sind gratis.

###  Krita 
Krita ist ein Zeichnungsprogramm, hier werde ich alle meine Assets gestalten. Ich werde hier also die Charaktere zeichnen, da Menu gestalten und die Cutscenes hier machen. Krita ist ein open Source Programm und somit gratis, ich benutze es schon seit längerem und kenne mich mit dem Programm ganz gut aus. Mit Krita kann ich sogar Animationen erstellen.

####  Indesign
Indesign werde ich dafür verwenden, um das Layout meines Spiel schnell und einfach darstellen kann.

#### Illustrator
Illustrator werde ich benutzen um das Logo für mein Spiel zu entwerfen. Ich könnte es auch mit Krita machen, aber hier geht es ein bisschen einfacher. Illustrator arbeitet mit Vector layern, anstatt mit Pixeln.

### Unity
Unity ist eine Laufzeit- und Entwicklungsumgebung für Spiele, also eine Spiel-Engine. Unity ist ebenfalls open source, das einzige ist, was wirklich Geld kostet sind "Assets" aus dem Unity Assets Store. Da ich alles selber zeichnen werde, muss ich auch hier keinen Cent ausgeben.

#### Visual code studio
Visual Code Studio hängt mit Unity zusammen. Hier wird werden die Skripts erstellt.

#### Inky
Ink ist ein skripting Sprache für Spiele. Inky ist der Editor, mit dem man am besten ink Datein bearbeitet. Ink ist spezialisiert für Spiel Dialogue und Konversationen, die Texte im Inky werden in einem JSON File umgewandelt.
