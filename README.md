# game_cat

School project, my project is creating a 2D pet simulation game on Unity. All the Assets would be drawn by me. 


# Inhaltsverzeichnis

[Bildergalerie](/Bildergalerie)



## Einführung
Ich habe mich entschieden für dieses Modul ein Spiel zu entwickeln. Ich habe schon oft über Game Jams gehört, Spiele die innerhalb von 72 Stunden entwicklt wurde. Wenn jemand in 72 Stunden ein Spiel entwickeln kann, dann schaffe ich das wohl auch in einem Monat. 

Ich habe zwar noch nie ein Spiel entwickelt und noch nie mit Unity gearbeitet, aber ich bin trotzdem zuversichtig.


## MoSCoW
MoSCoW steht für:
- Must Have
- Should Have
- Could Have
- Won't Have
Die Os sind nur dazu da um die aussprache zu vereinfachen. 
#### Must Have
- 1 Mini Game
- 2 Endings
- Health Bar 
- Game Over Screen
- Start screen 
#### Should Have
- Animations
- more mini games
- Tutorial
- Save Files
#### Could Have
- Choose Player Apaperance (male/female/nonbinary)
- more detailed Background

#### Won't Have
- Money System (Won't need money to buy food, no job system)

## IPERKA

### Informieren

Zuerst mal musste ich mich Informieren, was das Ziel dieses Projektes ist. Das Ziel ist es nicht, ein komplett fertiges Produkt
erschaffen zu können, sondern eher wie man das Produkt erschafft. 

Je nach dem was ich gerade für mein Spiel brauchte, holte ich mich andere Informationen. Wie man Dialogue anzeigen lässt, Bilder ladet oder
wie man eine Animation auf Unity hochlädt. Unten sind jeweils die Videos, welche mir am meisten geholfen haben:

video links:
Buttons/ Menu Unity:        https://youtu.be/zc8ac_qUXQY<br>
Pause Menu in Unity:        https://youtu.be/JivuXdrIHK0<br>
2D in Unity importieren:    https://youtu.be/hkaysu1Z-N8<br>
Unity Dialouge System:      https://youtu.be/_nRzoTzeyxU<br>
Pong programmieren          https://youtu.be/YHSanceczXY

### Planen


<img src="../png_files/gant.jpg" width="" height="">

Das Plannen war chaotisch, ich hatte mir zwar hier und da Deadlines gesetzt aber am Ende konnte ich nicht alle ganz einhalten.  
Hier waren die Deadlines welche ich mir gesetzt habe:

- 06.06.2022 Animationen + Buttons etc. gezeichnet
- 13.06.2022 Game Alpha Version abgeschlossen + testbereit
- 20.06.2022 Test durchgeführt + Verbesserungsvorschläge abgeschlossen
- 27.06.2022 fertig mit dem Spiel

Eine richtige "Alpha" Version konnte ich nie erstellen und generell ist das Spiel immer noch recht unfertig.


### Entscheiden
Wieso habe ich mich entschieden ein Spiel zu entwickeln?
Ich hatte insgesammt drei Ideen:
- [ ] Website
- [ ] Video drehen über ein Informatik Thema
- [x] Ein Spiel Entwickeln

#### Nutzwertanalyse

### Realisieren
In diesem Projekt kam Informieren und Realisieren Hand in Hand. Beim realisieren habe ich zuerst meine "Assets" gestaltet, bzw. Cutscenes
gezeichnet, die Animationen erstellt und die Buttons gestaltet. Jedesmal wenn es nun darum ging auf Unity etwas zu machen, 
habe ich in der Regel immer ein Tutorial Video befolgt. 


<img src="../png_files/nutzwerkanalyse.jpg" width="" height="">

### Kontrollieren 
Checkliste:

- [x] Bilderqualität stimmen?
- [ ] Funktioniert der Coder/Skript?
- [x] Kann man es spielen?
- [x] Gibt es zwei Endings?
- [x] Gibt es mindestens ein Mini-Game?

#### Bilder/ Animation
- [x] Stimmt Bilderqualität
- [x] Loopt Animation?
- [ ] Änder sich die Animation (Ist Animation mit einem Skript verbunden?)

#### Spiel/ Text/ Buttons
- [x] Funktionieren die Buttons?
- [x] Tauchen die Buttons im richtigem Moment auf?
- [x] Ist der Text in der Text Box?


####  Defekt auflisten:
##### Kritikal
Das "Main Spiel" funktioniert nicht wirklich, es gibt kein Skript der sich "merkt", wie sich das Mädchen gerade fühlt.
##### high
Wenn man das Mädchen anspricht, verschwindet die Menu Box nicht automatisch.

### Auswerten

Im Nachhinein gibt es viele Dinge die ich das nächste mal besser machen kann. Wenn ich jemals wieder mit Unity ein Spiel entwickeln werde,
werde ich bestimmt viel schneller sein, da ich jetzt nun mehr Erfahrung habe.
Wo ich am meisten Zeit gebraucht habe waren die Dialoge. Ich hatte enorme Schwierigkeiten mit dem Layout und mit dem Buttons. Zuerst 
habe ich es einfach mit Unity und Skripts erstellt, später habe ich INK entdeckt und habe das benutzt. Leider hatte ich das Problem mit INK
ein vernünftiges Layout zu erstellen, Die Knöpfe erschienen eben oft am falschen Ort.


