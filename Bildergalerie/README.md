# Bildergalerie
 [TOC]

# Cutscenes

</br>
<img src="../png_files/cutscenes/scene1.png" width="300" height="180">
</br>

Hier ist die erste Cutscenes des Spieles, in dem man das kleine Mädchen findet. Hier kommt auch schon die grosse Entscheidung, welche das Spiel schon zu einem Ende führen kann.

<img src="../png_files/cutscenes/scene2.png" width="300" height="180">

Diese Sequenz speielt nur ab, wenn man sich vorher entscieden hat, sich nicht um das Mädchen zu kümmern. Der Spieler läuft einfach an ihr vorbei.

<img src="../png_files/cutscenes/scene3.png" width="300" height="180">

Das ist das erste "Ending" vom Spiel. Funfact, ich habe es als "Ending 0 Abonded" bezeichnet.

<img src="../png_files/cutscenes/scene4.png" width="300" height="180">

Wenn man jetzt chronologisch geht, ist das hier "Ending 1 Death", bzw. der GAME OVER Screen in dem sie stirbt. Ich wollte nicht ihre Leiche zeichnen, also habe ich ein Altar gezeichnet. Der Spieler hat sich zwar nicht um sie gekümmert, aber hatte zumindest genug Anstand ihr eine vernünftige Beerdigung zu erichten. Jetzt stellt sich die Frage ob man verhaftet wird oder nicht? Leider nicht, da ich das nicht programmiert habe.

<img src="../png_files/cutscenes/scene5.png" width="300" height="180">

Das einzig wahre Happy End, Ending 3. Man hat das kleine Mädchen ins Herz geschlossen und man kümmert sich nun um sie.

</br>

# Skizzen
</br>

<img src="../png_files/sketches/sketch1.jpg" width="250" height="300">
</br>

Erste Ideen was mein Projekt sein soll. Hier habe ich zum ersten mal das kleine Mädchen skizziert und wie das Spiel allgemein auseehen soll. Unten rechts sieht man auch schon wie ich die MoSCoW <link> Methode verwendet habe. (Die Zeichnung oben rechts kann man ignorieren, die hat nichts mit dem Projekt zu tun)

<img src="../png_files/sketches/sketch2.jpg" width="250" height="300">
</br>

Kleines Thumbnail für das Spiel und ein paar Mimiken vom Mädchen. 

<img src="../png_files/sketches/sketch3.jpg" width="250" height="300">
</br>

Ideen was für verschiedene Enden es gäben könnte, je nach dem wie gut man sich um sie kümmert.

<img src="../png_files/sketches/sketch4.jpg" width="250" height="300">
</br>

Mehr Skizzen über das kleine Mädchen. Ich hab mir ein paar Gedanken zu ihrem Kleid und Augen gemacht. Sie soll möglichst süss sein und einfach zu zeichnen.

<img src="../png_files/sketches/sketch5.jpg" width="250" height="300">
</br>

MoSCoW weiter ausgeschrieben.

<img src="../png_files/sketches/sketch6.jpg" width="250" height="300">
</br>

Ideen was der Titel für das Spiel sein könnte.

<img src="../png_files/sketches/sketch7.jpg" width="250" height="300">
</br>

Skizzen von der erste Cutscene + die erste Entscheidung die der Spieler machen kann, die schon zu einem Ende führen kann. Rechts habe ich den Spieler skizziert. Ich habe mir überlegt ob ich einen weiblichen oder männlichen Spieler machen soll, für jetzt habe ich einfach einen geschlechts neutrale Person gezeichnet.

